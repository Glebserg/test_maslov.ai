# Test task

## Task 1
TODO:
- проставь энвы (см. .env.example)
- запусти постгрю (тестилось на 15й версии, но другая тоже подойдет) и создай базу
- прогони миграции
- доделай ручку, к которой написан TODO комментарий

Как запустить приложение, прогнать линтер и миграции - см. Makefile

За помощью обращайся к чему угодно, но начать лучше с документации:
- [poetry](https://python-poetry.org/)
- [yoyo](https://ollycope.com/software/yoyo/latest/)
- [strawberry](https://strawberry.rocks/docs)
- [graphql](https://graphql.org/learn/)
- [fastapi](https://fastapi.tiangolo.com/)
- [asyncpg](https://magicstack.github.io/asyncpg/current/)
- [ruff](https://docs.astral.sh/ruff/)
- [mypy](https://mypy.readthedocs.io/en/stable/getting_started.html)


## Stack

- python:3.12, FastAPI, GraphQL, pydantic
- PostgreSQL, psycopg
- SQL migrations, poetry
- docker, docker-compose

# How to run the project
## option 1 (docker)
- [ ]  Clone the repository:
```bash
git clone git@gitlab.com:Glebserg/test_maslov.ai.git
```
- [ ] Copy* `.env.example` to `.env`
```bash
cp .env.example .env
```
> (*) - if you have Windows, then
```bash
copy .env.example .env
```
- [ ] Edit the file if you needed (e.g., choose ports)
- [ ] Launch command
```bash
docker compose up --build
```
After the launch, visit [127.0.0.1:11111/docs](http://127.0.0.1:11111/docs) `(localhost:${API_PORT}/docs)`
to view the rout in swagger or [127.0.0.1:11111/graphql](http://127.0.0.1:11111/graphql) to learn schema graphql.

## Summary
Two containers will be launched: api, postgres in one Docker network and will communicate using container names.
A migrations will run to the database, and you can interact with the API.

## option 2 (local: if you already have any database)
```bash
uvicorn schema:app
```