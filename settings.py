import os

import pydantic
from pydantic_settings import BaseSettings
from dotenv import load_dotenv

load_dotenv()


class PreSettings(BaseSettings):
    DB_USER: str = pydantic.Field(alias="POSTGRES_USER")
    DB_PASSWORD: str = pydantic.Field(alias="POSTGRES_PASSWORD")
    DB_NAME: str = pydantic.Field(alias="POSTGRES_DB_NAME")


class DockerSettings(PreSettings):
    DB_SERVER: str = pydantic.Field(alias="POSTGRES_SERVICE_NAME")
    DB_PORT: int = 5432


class LocalSettings(PreSettings):
    DB_SERVER: str = pydantic.Field(alias="POSTGRES_HOST")
    DB_PORT: int = pydantic.Field(alias="POSTGRES_PORT")


if os.environ.get("DOCKER_ENVIRONMENT"):
    class Settings(DockerSettings, PreSettings):
        pass
else:
    class Settings(LocalSettings, PreSettings):
        pass
