from typing import List, Dict, Union

from contextlib import asynccontextmanager
from functools import partial
from fastapi import FastAPI
import strawberry
from strawberry.types import Info
from strawberry.fastapi import BaseContext, GraphQLRouter
from databases import Database

from settings import Settings


class Context(BaseContext):
    db: Database

    def __init__(
        self,
        db: Database,
    ) -> None:
        self.db = db


@strawberry.type
class Author:
    id: int
    name: str


@strawberry.type
class Book:
    id: int
    title: str
    author: Author


@strawberry.type
class Query:

    @strawberry.field
    async def books(
            self,
            info: Info[Context, None],
            author_ids: List[int] | None = None,
            search: str | None = None,
            limit: int | None = None,
    ) -> List[Book]:
        query = """
            SELECT books.id, books.title, authors.id as author_id, authors.name as author_name
            FROM books
            LEFT JOIN authors ON books.author_id = authors.id
            WHERE 1=1
        """
        params: Dict[str, Union[int, str, List[int]]] = {}

        if author_ids:
            query += " AND authors.id = ANY(:author_ids)"
            params["author_ids"] = author_ids

        if search:
            query += " AND books.title ILIKE :search"
            params["search"] = f"%{search}%"

        if limit:
            query += " LIMIT :limit"
            params["limit"] = limit

        rows = await info.context.db.fetch_all(query=query, values=params)
        books = [
            Book(
                id=row["id"],
                title=row["title"],
                author=Author(id=row["author_id"], name=row["author_name"]),
            )
            for row in rows
        ]

        return books

    @strawberry.field
    async def booksCount(
            self,
            info: Info[Context, None],
            author_ids: List[int] | None = None,
            search: str | None = None,
    ) -> int:
        query = "SELECT COUNT(*) FROM books JOIN authors ON books.author_id = authors.id WHERE 1=1"
        params: Dict[str, Union[str, List[int]]] = {}

        if author_ids:
            query += " AND authors.id IN :author_ids"
            params["author_ids"] = list(author_ids)

        if search:
            query += " AND books.title ILIKE :search"
            params["search"] = f"%{search}%"

        rows = await info.context.db.fetch_all(query=query, values=params)
        count = rows[0]["count"]

        return count


CONN_TEMPLATE = "postgresql+asyncpg://{user}:{password}@{host}:{port}/{name}"
settings = Settings()  # type: ignore
db = Database(
    CONN_TEMPLATE.format(
        user=settings.DB_USER,
        password=settings.DB_PASSWORD,
        port=settings.DB_PORT,
        host=settings.DB_SERVER,
        name=settings.DB_NAME,
    ),
)


@asynccontextmanager
async def lifespan(
    app: FastAPI,
    db: Database,
):
    async with db:
        yield

schema = strawberry.Schema(query=Query)
graphql_app = GraphQLRouter(  # type: ignore
    schema,
    context_getter=partial(Context, db),
)

app = FastAPI(lifespan=partial(lifespan, db=db))
app.include_router(graphql_app, prefix="/graphql")

# Additional Swagger documentation for GraphQL endpoint
app.openapi_schema = {
    "openapi": "3.0.0",
    "info": {
        "title": "Your API",
        "version": "1.0",
    },
    "paths": {
        "/graphql": {
            "post": {
                "summary": "GraphQL",
                "description": "Send a GraphQL query using POST",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "example": {"query": "query { books { id, title, author { id, name } } }"},
                        }
                    },
                },
                "responses": {
                    "200": {
                        "description": "Successful response",
                        "content": {
                            "application/json": {
                                "example": {"data": {"books": [{"id": 1, "title": "Book 1", "author": {"id": 1, "name": "Author 1"}}]}}
                            }
                        },
                    }
                },
            }
        }
    },
}
